# Webpack Intro

[Credit](https://www.youtube.com/watch?v=MpGLUVbqoYQ)

## Notes:

### 2 main things webpack does:

- it bundles our code/assets together (into one or a few files depending in configuration)
- it manages dependencies

---

### Getting Started

```
npm i --save-dev webpack webpack-cli
```

To use webpack either use the

```
webpack [COMMAND]
```

command in the terminal or add it to a script in package.json (can see it on start or webpack script in this repo).

To see webpack in action run:

```
npm run webpack
```

and then look at what was created in the "/dist" directory

---

### After installing:

1. Create webpack.config.json file in repos main directory. (see file for more details)
2. In the the webpack.config.json:
   - setup entry file (./src/index.js in this repo) where the webpack will start it's compilation
   - setup output file (eg. ./dist/main.js)
   - include script tag in index.html (eg. npm run webpack or something equivalent)
   - set mode (either devlopment or production, good practice to have seperate config files for development and production and specify the config file on the script in package.json) - dev and production modes minify code in different ways into your output file

---

### LOADERS:

Loaders are used to preprocess code that is not javascript (eg css, svgs, etc.)
Loaders can be found at:

- https://webpack.js.org/loaders/#root
- https://github.com/webpack-contrib/awesome-webpack#loaders

#### Loader examples

##### CSS

To inlude css in webpack:

```
npm i --save-dev style-loader css-loader
```

The css-loader preprocesses the css and puts it into output file as JS. The
style-loader injects the processed css into the DOM by adding a style tag. Webpack can be configured to seperate the css into it's own file so that there isn't one massive style tag on the html tree. (Will cover this later on.)

Next step is to add to webpack.config.js:

```javascript
...,
module: {
  rules: [
    {
      test: /\.css$/,
      use: ["style-loader", "css-loader"],
    },
  ],
},
...,
```

**style-loader must come before css-loader**

---

##### SASS

To include, add sass-loader to end of use rules array in webpack.config.json and change then test regex to /\.scss\$/ files.

Also

```
npm i --save-dev sass-loader
```

---

### CACHE BUSTING & PLUGINS

If the output file for webpack is just a static filename like "main.js", then depending on differing cache settings on different machines, browsers, etc. then the new preprocessed code may not update if the filename has not changed.

Webpack will add a contentHash for us out of the box if you change the output filename to something like:

```javascript
output: {
    filename: "main.[contentHash].js",
    path: path.resolve(__dirname, "dist")
  }
```

**Webpack will replace the square brackets with the content hash but the rest of the filename can be anything you want like "random-name-[contentHash]-extra-stuff-here.js"**

A problem that then arises is you then have to manually add/change the src attribute of the preproceesed code script tag in the html to match the new contentHash every time a change is made to the codebase.

To avoid this problem we can use a **plugin** that will preprocess our index.html for us.

---

#### Plugins

plugins found:

- https://webpack.js.org/plugins/
- https://github.com/webpack-contrib/awesome-webpack#webpack-plugins

plugins docs:

- https://webpack.js.org/concepts/plugins/

#### Plugins Examples

#### HtmlWebpackPlugin:

This plugin simplifies creation of html files to serve your webpack bundles. It is especially useful to include the new contentHash in the src attribute of the html script tag which changes with every compilation. (I.e solves the problem alluded to above)
The plugin can either generate the file for you or you can supply a template.

Usage:

```
npm i --save-dev html-webpack-plugin
```

Then require it in config file and then add it to plugins field in config object.

```javascript
plugins: [
   new HtmlWebpackPlugin(),
 ],
```

With a Template:

The template is a copy of the original index.html into ./src/template.html with the script tags and bootstrap tags removed. The webpack plugin will add this for us.

Then go into config and tell the plugin which template to use.

```javascript
plugins: [
    new HtmlWebpackPlugin({
      template: "./src/template.html",
    }),
  ],
```

The result of this plugin will be the output of a new "index.html" file which will appear in the configured output file ("/dist/index.html" in this case.)
Every time the command is run the "/dist/index.html" file will be **overwritten** to include the new contentHash.

_At this point you could delete the "index.html" in the main directory but I will leave it there for the sake of understanding these notes._

---

### Splitting Development and Production Configuration

A good way to manage this is to split the config among three files. One for shared configuration and then one each for dev and prod specific config.

This can give obvious advantages like setting up a hot reload server for development or minifying and seperating out a css file in production.

_Again for the sake of theses notes I will leave the webpack.config.json file as it is and create new different config files from now on. Normally the convention would probably be to use the webpack.config.js file as the shared configuration file but I have made a webpack.common.js file instead_

To merge webpack configs:

```
npm i --save-dev webpack-merge
```

The package provides a merge function which can be seen in action in webpack.dev.js and webpack.prod.js files.

The next step is to update package.json scripts to use the correct config on the desired scripts.
In this case the dev config will run on the start script and prod on the build script. See "package.json" for details.

#### Setting up dev server

In this case we will use "webpack-dev-server" package but there are of course many other packages and approaches to setting this up. This is just an example.
Run:

```
npm i --save-dev webpack-dev-server
```

To use this package we won't actually edit the dev config file but change the start script command to use webpack-dev-server instead of webpack:

```json
  "scripts": {
    "start": "webpack --config webpack.dev.js",
    ...,
   },
```

becomes

```json
  "scripts": {
    "start": "webpack-dev-server --config webpack.dev.js --open",
    ...,
   },
```

the "--open" flag is an option to automatically open the dev server page. webpack-dev-server also won't actually create the "dist" folder like using the webpack command because instead it creates it all in memory so that one doesn't have to delete all the files manually every time.

---

## More Loaders & Plugins

A problem with the setup at this point is that the template.html hardcodes the path to the assets folder with our svg image in it. While this works when running the dev server (because it is all stored in memory) when opening the "/build/index.html" file in the browser, that is created by the production build script, the path to the image is wrong. We could update the path but then that might cause issues with the dev server but more importantly, when we run the prod build we actually want ship our compiled images and other assets with the build and have them be accessed programatically.

Steps:

1. Move assets to src
   - _For clarity, I am leaving the original assets file at the top level but the first step is to move this folder and it's contents into the /src directory however i will **copy** this file instead_
2. Update path in template for dev server
3. Two loaders can be used to help solve production build issues
   - First is html-loader
   ```
   npm i --save-dev html-loader
   ```
   - add rule to webpack.common.js to include loader. Test on .html files
   - This loader will use JS require to require images from specified assets folder. However when we run our scripts we find an error because webpack doesn't know how to deal with requiring and compiling files.
   - So we use file-loader to handle this
   ```
   npm i --save-dev file-loader
   ```
   - Then add new rule to config with options (see webpack.common.js)

---

Another problem we face is that everytime we run the build script and we have changed our code we get a new compiled js file with a different content hash. If we ran our script multiple times the /build directory would start to get very cluttered with unused js files. We can use a plugin called clean-webpack-plugin to solve this. This plugin will delete the build directory before webpack compiles the code and recreates the directory from scratch.

```
npm i --save-dev clean-webpack-plugin
```

- Since this is only a problem faced in the prod build add the plugin to prod config.

---

## Multiple Entrypoints

If the case rises where part of our code base relies on an external library we can split our javascript files into vendor and main js files. The benefit here being that the vendor file's code isn't likely to change as much as your own code during development so we can seperate them to improve build speed. Examples might include something like bootstrap, jquery etc.
To solve this we create multiple entrypoints in our common config file.

- For the example I have included a file /src/vendor.js to mimic the idea of having third party code. In vendor.js bootstrap is being imported
- in webpack.common.js change the entry field to an object with keys that associate to your output files
- then in dev and prod configs change the output filenames to have [name] instead of hardcoding "main.[contenthash].js"
- webpack will add the script tag of the extra js files to index.html for you
  - navbar from bootstrap included to show examples of this

---

## Extract CSS

Here is a common pattern found in projects is that it is good practice to extract css into their own css files instead of waiting for JS to add all the styles via a script. The problem one sees is that for a brief moment on the page load we can see the raw html being displayed with no styling applied before the css in injected. It can be quite slow and expensive to extract and bundle css so in this case it will be done only on the prod build

- First add the plugin:

```
npm i --save-dev mini-css-extract-plugin
```

- require and add to plugins array on prod config
- add filename option to plugin (optional)
- this creates extracted css file but we need another plugin to minify the css

```
npm i --save-dev optimize-css-assets-webpack-plugin
```

- add to prod config via the optimization.minimizers array. this overwrites the default js minifier so manually include the terser-webpack-plugin to minify the js
- we can also add minify options to html-webpack-plugin see prod config for details

---

## Extra Flags

Some useful extra flags to pass to webpack if needed.

Verbose information on the bundling process:

```
webpack --progress --config webpack.prod.js --display-modules --display-reasons --dispay-chunks
```

Watch and update the build as changes are made:

```
webpack --config webpack.prod.js --watch
```

Include source maps in the build for debugging purposes (ideal for development):

```
webpack --config webpack.dev.js -d
```

_-d is an alias for the flags --debug --devtool source-map --output-pathinfo_

Optimizing and minimizing flag (uglify):

```
webpack -p
```

_-p is an alias for the flags --optimize-minimize --optimize-occurence-order_

### Performance Tool:

```
webpack --profile --json >> stats.json
```

These flags will output a "stats.json" file of output build statistics which can be uploaded to webpack's [analyse](https://webpack.github.io/analyse/) tool

---

More advanced concepts to look into are discussed in this [video](https://www.youtube.com/watch?v=MzVFrIAwwS8)
