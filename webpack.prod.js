const path = require("path")
const common = require("./webpack.common")
const merge = require("webpack-merge")
const { CleanWebpackPlugin } = require("clean-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const OptimizeCssAssetsWebpackPlugin = require("optimize-css-assets-webpack-plugin")
const TerserWebpackPlugin = require("terser-webpack-plugin") // No need to install this as it comes with webpack
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = merge(common, {
  mode: "production",
  output: {
    filename: "[name].[contentHash].bundle.js", // name is replaced by the entrypoint keys (ie main or vendor)
    path: path.resolve(__dirname, "build") // path.resolve(__dirname, ...) used to programmatically get absolute filepath therefore can be used on anyones machine
  },
  optimization: {
    minimizer: [
      new OptimizeCssAssetsWebpackPlugin(), // note: this plugin is not added to plugins array but is used here in optimization. This overides the default JS minifier so it must be added back in manually
      new TerserWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: "./src/template.html",
        minify: {
          removeAttributeQuotes: true,
          collapseWhitespace: true,
          removeComments: true
        }
      })
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].[contentHash].css"
    }),
    new CleanWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader, // 3. Extract css into files
          "css-loader", // 2. Turns css into common JS
          "sass-loader" // 1. Turns sass into css
        ]
      }
    ]
  }
})
