module.exports = {
  // entry: "./src/index.js", // by default this will output a main build file from the specified file path
  entry: {
    main: "./src/index.js",
    vendor: "./src/vendor.js" // add secondary vendor entrypoint
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: ["html-loader"]
      },
      {
        test: /\.(svg|png|jpg|gif)$/,
        use: {
          // We can use this alternate syntax for loaders if we need to pass options to the loader instead of using the default options
          loader: "file-loader",
          options: {
            name: "[name].[hash].[ext]",
            outputPath: "imgs"
          }
        }
      }
    ]
  }
}
