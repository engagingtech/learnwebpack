const path = require("path")
const common = require("./webpack.common")
const merge = require("webpack-merge")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = merge(common, {
  mode: "development", // development and production have different minification methods
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist") // path.resolve(__dirname, ...) used to programmatically get absolute filepath therefore can be used on anyones machine
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          "style-loader", // 3. Inject styles into DOM
          "css-loader", // 2. Turns css into common JS
          "sass-loader" // 1. Turns sass into css
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/template.html"
    })
  ]
})
